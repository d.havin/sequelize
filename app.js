const express = require('express');
const { sequelize, User, Post } = require('./models');
const authMiddleware = require('./controllers/auth.controller').authMiddleware;
const authRouter = require('./routers/auth.router');
const profileRouter = require('./routers/profile.router');
const postRouter = require('./routers/post.router');
const friendsRouter = require('./routers/friends.router');

const app = express();
app.use(express.json());
app.use('/homepage', authRouter);
app.use('/profile', profileRouter); //authMiddleware
app.use('/posts', postRouter);      //authMiddleware
app.use('/friends', friendsRouter); //authMiddleware


app.get('/user/:uuid', async (req, res) => {
    const uuid = req.params.uuid
    try {
        const user = await User.findOne({ where: { uuid }, include : 'posts'})
        return res.json(user)
    } catch (err) {
        console.log(err)
        return res.status(500).json({ error: 'Something went wrong', message: err.message })
    }
});

app.delete('/user/:uuid', async (req, res) => {
    const uuid = req.params.uuid
    try {
        const user = await User.findOne({ where: { uuid } })
        await user.destroy()
        return res.json({message: 'User deleted'})
    } catch (err) {
        console.log(err)
        return res.status(500).json({ error: 'Something went wrong'})
    }
});

app.listen({ port: 5000 }, async () => {
    console.log('Server up on http://localhost:5000')
    await sequelize.authenticate();
    // await sequelize.sync({ force: true }) 
    console.log('Database Connected!!!')
});
