'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('friends', [{
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e130',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e129'
    },
    {
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e131',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e129'
    },
    {
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e127',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e128'
    },
    {
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e127',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e129'
    },
    {
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e129',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e130'
    },
    {
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e129',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e131'
    },
    {
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e128',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e127'
    },
    {
      friendId: 'd79a9b35-debf-4703-b142-3cf08643e129',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e127'
    },
  ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('friends', null, {});
  }
};
