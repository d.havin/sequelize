'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('users', [{
      name: 'John',
      surname: 'Hendrik',
      email: 'Polgre@mail.ru',
      uuid: 'd79a9b35-debf-4703-b142-3cf08643e127',
      avatar: 'https://www.partyfiesta.com/de/8209-large_default/minions-supershape-ballon.jpg',
      password: '2909'
    },
    {
      name: 'Arnold',
      surname: 'Mahmedovuch',
      email: 'Arny@mail.ru',
      uuid: 'd79a9b35-debf-4703-b142-3cf08643e128',
      avatar: 'https://www.partyfiesta.com/de/8209-large_default/minions-supershape-ballon.jpg',
      password: '2909'
    },
    {
      name: 'Andrey',
      surname: 'Yzyrbaev',
      email: 'Yzyrbaev@mail.ru',
      uuid: 'd79a9b35-debf-4703-b142-3cf08643e129',
      avatar: 'https://www.partyfiesta.com/de/8209-large_default/minions-supershape-ballon.jpg',
      password: '2909'
    },
    {
      name: 'Egor',
      surname: 'Letov',
      email: 'Letov@mail.ru',
      uuid: 'd79a9b35-debf-4703-b142-3cf08643e130',
      avatar: 'https://www.partyfiesta.com/de/8209-large_default/minions-supershape-ballon.jpg',
      password: '2909'
    },
    {
      name: 'Nick',
      surname: 'Spy',
      email: 'Spy@mail.ru',
      uuid: 'd79a9b35-debf-4703-b142-3cf08643e131',
      avatar: 'https://www.partyfiesta.com/de/8209-large_default/minions-supershape-ballon.jpg',
      password: '2909'
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', null, {});
  }
};
