'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('friendshiprequests', [{
      candidateId: 'd79a9b35-debf-4703-b142-3cf08643e130',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e128'
    },
    {
      candidateId: 'd79a9b35-debf-4703-b142-3cf08643e130',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e131'
    },
    {
      candidateId: 'd79a9b35-debf-4703-b142-3cf08643e128',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e131'
    },
    {
      candidateId: 'd79a9b35-debf-4703-b142-3cf08643e131',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e127'
    },
    {
      candidateId: 'd79a9b35-debf-4703-b142-3cf08643e128',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e129'
    },
    {
      candidateId: 'd79a9b35-debf-4703-b142-3cf08643e127',
      useruuid: 'd79a9b35-debf-4703-b142-3cf08643e130'
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('friendshiprequests', null, {});
  }
};
