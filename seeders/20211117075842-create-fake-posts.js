'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    await queryInterface.bulkInsert('posts', [{
      postuuid: "98eafcde-c0a9-4803-b281-3c3099cec000",
      description: "It's time. Come and enjoy from new minins part",
      image: "https://upload.wikimedia.org/wikipedia/vi/3/3d/Minions_poster.jpg",
      postOwner: 'd79a9b35-debf-4703-b142-3cf08643e127',
      createdAt: "2021-11-16 12:02:20.231+03",
    },
    {
      postuuid: "98eafcde-c0a9-4803-b281-3c3099cec111",
      description: "It's time. Come and enjoy from new minins part",
      image: "https://upload.wikimedia.org/wikipedia/vi/3/3d/Minions_poster.jpg",
      postOwner: 'd79a9b35-debf-4703-b142-3cf08643e127',
      createdAt: "2021-11-16 12:02:20.231+03",
    },
    {
      postuuid: "98eafcde-c0a9-4803-b281-3c3099cec222",
      description: "It's time. Come and enjoy from new minins part",
      image: "https://upload.wikimedia.org/wikipedia/vi/3/3d/Minions_poster.jpg",
      postOwner: 'd79a9b35-debf-4703-b142-3cf08643e129',
      createdAt: "2021-11-16 12:02:20.231+03",
    },
    {
      postuuid: "98eafcde-c0a9-4803-b281-3c3099cec333",
      description: "It's time. Come and enjoy from new minins part",
      image: "https://upload.wikimedia.org/wikipedia/vi/3/3d/Minions_poster.jpg",
      postOwner: 'd79a9b35-debf-4703-b142-3cf08643e129',
      createdAt: "2021-11-16 12:02:20.231+03",
    },
    {
      postuuid: "98eafcde-c0a9-4803-b281-3c3099cec444",
      description: "It's time. Come and enjoy from new minins part",
      image: "https://upload.wikimedia.org/wikipedia/vi/3/3d/Minions_poster.jpg",
      postOwner: 'd79a9b35-debf-4703-b142-3cf08643e131',
      createdAt: "2021-11-16 12:02:20.231+03",
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('posts', null, {});
  }
};
