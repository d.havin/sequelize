const express = require('express');
const postController = require('../controllers/post.controller');

const postRouter = express.Router();

postRouter.post('/createpost', postController.createPost);
postRouter.get('/allposts', postController.getAllPosts);
postRouter.delete('/delete', postController.deletePost);
postRouter.get('/myPosts', postController.getAllMyPosts);
postRouter.get('/myFriendsPosts', postController.getAllMyFriendsPosts);

module.exports = postRouter;