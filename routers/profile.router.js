const express = require('express');
const profileController = require('../controllers/profile.controller');

const profileRouter = express.Router();

profileRouter.put('/editInfo', profileController.editInfo);

module.exports = profileRouter; 