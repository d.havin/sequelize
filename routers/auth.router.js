const express = require('express');
const { check } = require('express-validator');
const authController = require('../controllers/auth.controller');

const authRouter = express.Router();

authRouter.post("/registration", [
    check('email', "email field cannot be empty").notEmpty(),
    check('password', "password length must be between 4 and 20 symbols").isLength({min:4, max: 20})
], authController.registration);

authRouter.post("/login", authController.authorization);
// authRouter.get('/auth', authMiddleware, getUser);

module.exports = authRouter;
