const express = require('express');
const friendsController = require('../controllers/friends.controller');

const friendsRouter = express.Router();

friendsRouter.post('/sendrequest', friendsController.sendRequest);
friendsRouter.delete('/cancelrequest', friendsController.cancelRequest);
friendsRouter.post('/applyinvitation', friendsController.applyInvitation);
friendsRouter.post('/rejectinvitation', friendsController.rejectInvitation);
friendsRouter.delete('/deletefriend', friendsController.removeFriend);
friendsRouter.get('/getmyfriends', friendsController.getAllFriends);
friendsRouter.get('/getrequests')
friendsRouter.get('/getinvitations')                //  <=  TBD


module.exports = friendsRouter;