const { User, Friend, FriendshipRequests, Invitations } = require('../models');

const sendRequest = async (req, res) => {
  const { userUuid, candidateUuid } = req.body;
  try {
    const user = await User.findOne({where: {uuid: userUuid}});
    const candidate = await User.findOne({where: {uuid: candidateUuid}});
    await FriendshipRequests.create({userId: user.id, candidateId: candidateUuid});
    await Invitations.create({userId: candidate.id, senderId: user.id});
    return res.status(200).json({ message: "Request has been sent" });
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: "Something went wrong" });
  }
};

const cancelRequest = async (req, res) => {
  const { userId, candidateId } = req.body;
  try {
    const user = await User.findOne({where: {id: userId}});
    await FriendshipRequests.destroy({where: {userId: user.id, candidateId}});
    return res.status(200).json({ message: "Request has been canceled" })
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: "Something went wrong" });
  }
};

const applyInvitation = async (req, res) => {
  try {
      const { userUuid, candidateUuid } = req.body;
    
      const user = await User.findOne({where: {uuid: userUuid}});
      const sender = await User.findOne({where: {uuid: candidateUuid}});

      await FriendshipRequests.destroy({userId: user.id, candidateId: candidateUuid});
      await Invitations.destroy({userId: candidate.id, senderId: user.id});

      await Friend.create({userId: user.id, candidateId: candidateUuid});
      await Friend.create({userId: user.id, candidateId: candidateUuid});
    return res.status(200).json({ message: "Invitation has been applied" })
  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: "Something went wrong" });
  }
};

const rejectInvitation = async (req, res) => {
  try {

  } catch (err) {
    console.log(err);
    return res.status(500).json({ message: "Something went wrong" });
  }
};

const removeFriend = async function ( //TBD
  req,
  res
) {
  const { userUuid, friendId } = req.body
  try {
    const user = await User.findOne({ where: { uuid: userUuid } });
    await Friend.destroy({ where: {} });
    if (!user) {
      return res
        .status(500)
        .json({ message: "Something went wrong. Cant find user" });
    }
    const candidate = await User.findOne({ _id: request.body._id });
    if (!candidate) {
      return res.status(500).json({ message: "Something went wrong" });
    }

    // Project.update(
    //   { title: 'a very different title now' },
    //   { where: { _id: 1 } }
    // )

    await User.updateOne(
      { _id: request.user._id },
      { $pull: { friends: request.body._id } }
    );
    await User.updateOne(
      { _id: request.body._id },
      { $pull: { friends: request.user._id } }
    );
    await candidate.save();
    await user.save();
    const users = await User.find({});
    const newUser = await User.findOne({ _id: request.user._id });
    const friendsList = newUser?.friends
    return response.json({ users: users, friendsList: friendsList });
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};



const getAllFriends = async (req, res) => {
  const { userUuid } = req.body
  try {
    const user = await User.findOne({ where: { uuid: userUuid } });
    const friends = await Friend.findAll({ where: { userId : user.id } })
    if (!user) {
      return res
        .status(500)
        .json({ message: "Something went wrong" });
    }
    return response.json(friends);
  } catch (err) {
    return response.status(500).json({ message: "Something went wrong" });
  }
};

module.exports = { sendRequest, cancelRequest, applyInvitation, rejectInvitation, getAllFriends, removeFriend }