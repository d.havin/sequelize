const { User } = require('../models');
const { validationResult } = require("express-validator");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const accessTokenSecret = 'youraccesstokensecret';
const generateAccessToken = (email, uuid) => {
    const payload = {
        email,
        uuid
    }
    return jwt.sign(payload, accessTokenSecret, { expiresIn: '240h' })
}

const registration = async function (req, res) {
    try {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            let err = errors;
            return res.status(400).send(`error message: ${err.errors[0].msg}`)
        }
        const { name, surname, email, password } = req.body
        let user = await User.findOne({ where: { email } });
        if (user) {
            return res.status(400).json({ message: "This email is already in use" })
        }
        const hashPassword = bcrypt.hashSync(password, 10)
        await User.create({ name, surname, email, password: hashPassword });
        return res.status(201).json({ message: "Registration successfully." });
    } catch (err) {
        console.log(err)
        return res.status(500).json({ message: "Something went wrong" })
    }
}

const authorization = async function (req, res) {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ where: { email }});
        if (!user) {
            return res.status(400).json({ message: `User with such ${email} is not founded` });
        }
        const validPassword = bcrypt.compareSync(password, user.password);
        if (!validPassword) {
            return res.status(400).json({ message: `Wrong password` });
        }
        const token = generateAccessToken(user.email, user.uuid)
        return res.status(200).json({ token: `Bearer ${token}`, user: user });
    } catch (err) {
        console.log(err)
        res.status(500).json({ message: "Something went wrong" });
    }
}

const authMiddleware  = (req, res, next) => {
    try {
        const authHeader = req.headers.authorization;
        if (authHeader){
            const token = authHeader.split(' ')[1];
            if (!token){
                return res.status(403).json({message: "User is not logged in"})
            }
            const user = jwt.verify( token, accessTokenSecret )
            req.user = user
            next()
        }
    }
    catch (err) {
        return res.status(403).json({message: "User is not logged in"})
    }
}

module.exports = { registration, authorization, authMiddleware };