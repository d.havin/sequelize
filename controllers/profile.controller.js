const { User } = require('../models');

const editInfo = async (req, res) => {
    const { uuid, name, surname, email, password, avatar } = req.body;
    try {
        await User.update({
            name: name,
            surname: surname,
            email: email,
            password: password,
            avatar: avatar
        }, { where: { uuid } })
        const updatedUser = await User.findOne({ where: { uuid } })
        return res.status(200).json(updatedUser)
    } catch (err) {
        console.log(err)
        return res.status(500).json({ error: 'Something went wrong' })
    }
};

module.exports = { editInfo };
