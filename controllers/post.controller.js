const { Post, User, Friend  } = require('../models');

const createPost = async (req, res) => {
    const { userUuid, description, image } = req.body;
    try {
        const user = await User.findOne({ where: { uuid: userUuid } });
        const post = await Post.create({ description, image, userId: user.id });
        return res.status(200).json(post);
    } catch (err) {
        console.log(err)
        return res.status(500).json(err);
    }
}

const deletePost = async (req, res) => {
    const { userUuid, postUuid } = req.body;
    try {
        const post = await Post.findOne({ where: { uuid: postUuid } });
        const user = await User.findOne({ where: { uuid: userUuid } });
        if (!post) {
            return response.status(500).json({ message: "Can't find this post." });
        }
        if (!user) {
            return response.status(500).json({ message: "Something went wrong." });
        }
        else {
            if (post.userId.toString() !== user.id.toString()) {
                return res.status(400).json({ message: "It's not your post and you don't have access to delete this post." });
            } else {
                await post.destroy();
                return res.json({ message: 'Post deleted' });
            }
        }
    } catch (err) {
        console.log(err)
        return res.status(500).json({ error: 'Something went wrong' });
    }
} 

const getAllPosts = async (req, res) => {
    try {
        const posts = await Post.findAll({include: 'user'})
        if (!posts){
            return res.status(500).json({message: "No posts"})
        }
        return res.status(200).json(posts)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
}

const getAllMyPosts = async (req, res) => {
    const { userUuid } = req.body;
    try {
        const user = await User.findOne({where: {uuid: userUuid}})
        const posts = await Post.findAll({where: {userId: user.id}})
        if (!posts){
            return res.status(500).json({message: "No posts"})
        }
        return res.status(200).json(posts)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
}

const getAllMyFriendsPosts = async (req, res) => {
    const { userUuid } = req.body;
    try {
        const user = await User.findOne({where: {uuid: userUuid}})
        const friends = await Friend.findAll({where: {}})        // <== TBD!!!

        const posts = await Post.findAll({where: {userId: user.id}})
        if (!posts){
            return res.status(500).json({message: "No posts"})
        }
        return res.status(200).json(posts)
    } catch (err) {
        console.log(err)
        return res.status(500).json(err)
    }
}

module.exports = { getAllPosts, createPost, deletePost, getAllMyPosts, getAllMyFriendsPosts }