'use strict';
module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('friendshiprequests', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      candidateId: {
        type: DataTypes.STRING,
        allowNull: false
      },
      userId: {
        allowNull: false,
        type: DataTypes.INTEGER,
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('friendshiprequests');
  }
};