'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Invitations extends Model {

    static associate({ User }) {
      this.belongsTo(User, { foreignKey: 'userId', as: 'requestReceiver' })
    }
  };
  Invitations.init({
    senderId: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false,
    sequelize,
    tableName: 'invitations',
    modelName: 'Invitations',
  });
  return Invitations;
};