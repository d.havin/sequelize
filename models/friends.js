'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Friend extends Model {

    static associate({ User }) {
      this.belongsTo(User, { foreignKey: 'userId', as: 'friends' })
    }
    toJSON(){
      return { ...this.get(), id: undefined }
    }
  };

  Friend.init({
    friendid: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false,
    sequelize,
    tableName: 'friends',
    modelName: 'Friend',
  });
  return Friend;
};