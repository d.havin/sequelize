'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FriendshipRequests extends Model {

    static associate({ User }) {
      this.belongsTo(User, { foreignKey: 'userId', as: 'myRequests' })
    }
  };
  FriendshipRequests.init({
    candidateId: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false,
    sequelize,
    tableName: 'friendshiprequests',
    modelName: 'FriendshipRequests',
  });
  return FriendshipRequests;
};