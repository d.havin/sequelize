'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    static associate({ Post, Friend, FriendshipRequests, Invitations }) {
      this.hasMany(Post, { foreignKey: 'userId', as: 'posts' })
      this.hasMany(Friend, { foreignKey: 'userId', as: 'friends' })
      this.hasMany(FriendshipRequests, { foreignKey: 'userId', as: 'friendrequests' })
      this.hasMany(Invitations, { foreignKey: 'userId', as: 'invitations' })
    }
    toJSON() {
      return { ...this.get(), id: undefined, password: undefined }
    }
  };

  User.init({
    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: { msg: 'User should have a name' },  
        notEmpty: { msg: 'Name should not be empty' }
      }
    },
    surname: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: { msg: 'Surname should have a surname' },
        notEmpty: { msg: 'Surname should not be empty' }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: { msg: 'User should have a email' },
        notEmpty: { msg: 'email should not be empty' },
        isEmail: { msg: 'Should be a valid email address' }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: { msg: 'New user should have a password' },
        notEmpty: { msg: 'Password should not be empty' }
      }
    },
    avatar: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'https://www.partyfiesta.com/de/8209-large_default/minions-supershape-ballon.jpg'
    },

  }, {
    timestamps: false,
    sequelize,
    tableName: 'users',
    modelName: 'User',
  });
  return User;
};